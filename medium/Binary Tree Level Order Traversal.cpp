/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    //ITERATIVE
    /*
    vector<vector<int>> levelOrder(TreeNode* root) {
        if(!root)
            return {};
        
        queue<TreeNode*> q;
        vector<vector<int>> output;
        
        output.emplace_back(vector<int>{root->val});
        q.push(root);
        while(q.size()){            
            vector<int> row;
            
            for(int i = q.size()-1; i>=0; i--){
                TreeNode* t = q.front();
                q.pop();
                if(t->left){
                    q.push(t->left);
                    row.emplace_back(t->left->val);
                }
                if(t->right){
                    q.push(t->right);
                    row.emplace_back(t->right->val);
                }
            }
            if(row.size() > 0)
                output.emplace_back(row);
        }
        return output;
    }*/
    
    //RECURSIVE
    
    vector<vector<int>> levelOrder(TreeNode* root) {
        buildVector(root, 0);
        return ret;
    }
    
    void buildVector(TreeNode* root, int depth){
        if(!root) return;
        if(ret.size() == depth) ret.emplace_back(vector<int>{}); //e.g. size=0 depth=0
        
        ret[depth].emplace_back(root->val);
            
        buildVector(root->left, depth+1);
        buildVector(root->right, depth+1);
    }
private:
    vector<vector<int>> ret = {};
};