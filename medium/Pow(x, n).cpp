class Solution {
public:
    //RECURSIVE
    /*
    double myPow(double x, int n) {
        if(n==0) return 1;
        if(n==numeric_limits<int>::min()){
            x *= x;
            n /= 2;
        }
        if(n<0) {
            n = -n;
            x = 1/x;
        }
        if(n%2 == 0) return myPow(x*x, n/2);
        else return x * myPow(x*x, (n-1)/2);
    }
    */
    
    //ITERATIVE
    double myPow(double x, int n) {
        double output = 1;
        if(n==numeric_limits<int>::min()){
            x *= x;
            n /= 2;
        }
        if(n < 0){
            n = -n;
            x = 1/x;
        }
        while(n){
            if(n%2==0){
                x *= x;
                n = n >> 1;
            } else{
                output *= x;
                
                x *= x;
                n = (n-1) >> 1;
            }
        }
    return output;
    }
};