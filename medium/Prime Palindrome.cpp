class Solution {
public:
    // Time Limit Exceeded
    /*
    int primePalindrome(int N) {
        for(int i = N; i<std::pow(10, 8); i++)
        {
            if(isPalimdromic(i))
                if(isPrime(i)) 
                        return i;
        }
        return -1;
    }
    
    bool isPalimdromic(int i){
        //std::string s = std::to_string(i);
        //return std::equal(s.begin(), s.begin()+s.size()/2, s.rbegin());
        
        int reversed = 0;
        int tmp = i;
        while(i){
            reversed = reversed*10 + i%10;
            i /=10;
        }
        return reversed == tmp;
    }
    */
    
    int primePalindrome(int N) {
        if(N <= 2) return 2;
        if(N >= 8 && N <= 11) return 11; 
        for(int i = 1; i<std::pow(10, 8); i++)
        {
            int tmp = getNextPalindrome(i);
                if(tmp >= N && isPrime(tmp)) 
                    return tmp;
        }
        return -1;
    }
    
    bool isPrime(int x){
        if(x == 2) return true;
        if(x <= 1 || x%2==0) return false;

        for(int i=3; i<((int)std::sqrt(x))+1; i+=2){
            if(x % i == 0) return false;
        }
        return true;
    }
    
    int getNextPalindrome(int x){
        //29% - 100%
        /*std::string s = to_string(x);
        std::string t = s;
        
        s.pop_back();
        std::reverse(t.begin(), t.end());
        return stol(s+t);*/
        
        //96% - 100%
        int output = x;
        output /= 10; // string(output).pop_back()
        while(x){
            output = output*10 + (x%10);
            x /= 10;
        }
        return output;
    }
    
};