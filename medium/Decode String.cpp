class Solution {
public:
	std::string decodeString(std::string s) {
		int repetitionNumber = 0;
		std::string charactersBetweenBrackets = "";

		std::stack<std::pair<unsigned int, std::string>> stack;

		// I wasn't sure if I could create my own functions, 
		// so for code readability I decided to create labda expressions

		// Parses character to integer
		auto parseInt = [](int currentRep, const char& s) {
			currentRep *= 10;
			currentRep += (s - '0');

			return currentRep;
		};

		// Returns a repeated n times string. 
		auto repeatString = [](const std::string& charactersBetweenBrackets, unsigned int n) {
			std::string repeatedString_temp;
			// lets allocate space once, not during each concatenation
			repeatedString_temp.reserve(charactersBetweenBrackets.size() * n);

			// concatenation loop
			for (unsigned int i = 0; i < n; i++) {
				repeatedString_temp += charactersBetweenBrackets;
			}

			return repeatedString_temp;
		};



		for (unsigned int i = 0; i < s.size(); i++) {
			// if character is a digit
			if (isdigit(s[i])) {
				repetitionNumber = parseInt(repetitionNumber, s[i]);
			}
			// if character is an open square bracket
			else if (s[i] == '[') {
				// stash previous data
				stack.push(std::make_pair(repetitionNumber, charactersBetweenBrackets));

				repetitionNumber = 0;
				charactersBetweenBrackets = "";
			}
			// if character is a close square bracket
			else if (s[i] == ']') {
				// take stashed data and pop them from stack
				auto[repetition, stashedCharacters] = stack.top();
				stack.pop();

				std::string repeatedString = repeatString(charactersBetweenBrackets, repetition);

				// add stashed characters
				charactersBetweenBrackets = stashedCharacters + repeatedString;
			}
			// if character is alphabetic
			// there is 'isalpha' function, but we assume that input string is correct
			else {
				charactersBetweenBrackets += s[i];
			}
		}
		return charactersBetweenBrackets;
	}
};