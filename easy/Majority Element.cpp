class Solution {
public:
    // #1
    /*
    int majorityElement(vector<int>& nums) {
        unordered_map<int, int> map;
        for(int i : nums){
            map[i]++;
        }
        int bestKey = 0, bestValue = 0;
        for(auto[key, value] : map)
            if(value > bestValue){
                bestValue = value;
                bestKey = key;
                if(bestValue >= nums.size()/2)
                    return bestKey;
            }
        return bestKey;
    }
    */
    
    //#2
    int majorityElement(vector<int>& nums) {
        unordered_map<int, int> map;
        for(int i : nums){
            if(++map[i] >= ceil(((double)nums.size()/2))){
                return i;
            };
        }
        return -1;
    }
};