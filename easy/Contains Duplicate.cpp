class Solution {
public:
    // #1
    /*
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> set;

        for(int i : nums){
            if(set.find(i) != set.end()) return true;
            set.insert(i);
        }
        return false;
    }
    */

    bool containsDuplicate(vector<int>& nums) {
        return nums.size() > std::unordered_set<int>(nums.begin(), nums.end()).size();
    }
};

