class Solution {
public:
    // #1 Not very efficient
    /*
    int repeatedNTimes(vector<int>& A) {
        for(int i : A){
            if(++map[i] >= A.size()/2)
                return i;
        }
        return -1;
    }
private:
    unordered_map<int, int> map;
    */
    
    int repeatedNTimes(vector<int>& A) {
        for(int i = 2; i<A.size(); i++){
            if(A[i] == A[i-1] || A[i] == A[i-2])
                return A[i];
        }
        return A[0]; //special case when [x, 1, 2, x]
    }
};