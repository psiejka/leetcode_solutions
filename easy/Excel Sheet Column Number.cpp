class Solution {
public:
    //ITERATIVE
    /*
    int titleToNumber(string s) {
        int output = 0;
        for(int i=0; i<s.size(); i++){
            output += ((int(s[i]))-64) * pow(26,s.size()-i-1);
        }
        
        return output;
    }
    */
    
    //RECURSIVE
    int titleToNumber(string s) {
        if(!s.size()) return 0;
        return (((int(s[0]))-64) * pow(26,s.size()-1)) + titleToNumber(s.substr(1));
    }
};