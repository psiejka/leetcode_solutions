class Solution {
public:
    bool isAnagram(string s, string t) {
        for(char c : s){
            map[c]++;
        }
        for(char c : t){
            if(--map[c] < 0) return false;
        }
        for(auto&[key, value] : map){
            if(value > 0) return false;
        }
        return true;
    }
private:
unordered_map<int, int> map;
};