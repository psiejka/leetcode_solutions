/*
class MinStack {
public:
    // 5% / 5%
    
    // initialize your data structure here.
    MinStack() {
        
    }
    
    void push(int x) {
        m_head = std::make_unique<S>(x, std::move(m_head));
    }
    
    void pop() {
        m_head = std::move(m_head->m_next);
    }
    
    int top() {
        return m_head->m_data;
    }
    
    int getMin() {
        int min = INT_MAX;
        S* head = m_head.get();
        while(head != nullptr){
            if(head->m_data < min)
                min = head->m_data;
            head = head->m_next.get();
        }
        return min;
    }
private:
    struct S{
        int m_data;
        std::unique_ptr<S> m_next;
        S(int data, std::unique_ptr<S> next) : m_data(data), m_next(std::move(next)) {}
    };
    std::unique_ptr<S> m_head = nullptr;
};

*/

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */

// 5% / 50%
class MinStack {
public:
    
    // initialize your data structure here.
    MinStack() {
        
    }
    
    ~MinStack(){
        while(m_head){
            S* tmp = m_head;
            m_head = m_head->m_next;
            delete tmp;
        }
    }
    
    void push(int x) {
        if(x <= min){
            m_head = new S(min, m_head);
            min = x;
        }
        m_head = new S(x, m_head);
    }
    
    void pop() {
        if(m_head){
            S* tmp = m_head;
            m_head = m_head->m_next;
            if(tmp->m_data == min){
                min = m_head->m_data;
                S* minP = m_head;
                m_head = m_head->m_next;                
                delete tmp;
                delete minP;
            } else{
                delete tmp;
            }
        }
    }
    
    int top() {
        return m_head->m_data;
    }
    
    int getMin() {
        return min;
    }
private:
    struct S{
        int m_data;
        S* m_next;
        S(int data, S* next) : m_data(data), m_next(next) {}
    };
    S* m_head = nullptr;
    int min = INT_MAX;
};