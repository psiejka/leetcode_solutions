class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if(nums.size() < 1) return nums.size(); 
        int lastInput = 1;
        for(int i=1; i<nums.size(); i++){
            if(nums[i] != nums[lastInput-1]){
                nums[lastInput] = nums[i];
                lastInput++;
            }
        }
        return lastInput;
    }
};