/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    // #1
    /*
    void deleteNode(ListNode* node) {
        while(node->next->next != NULL){
            node->val = node->next->val;
            node = node->next;
        }
        ListNode* t = node->next;
        node->val = node->next->val;
        node->next = NULL;
        delete t;
    }
    */
    
    // #2
    void deleteNode(ListNode* node) {
        ListNode* tmp = node->next;
        *node = *tmp; // nadpisuję dane node'a z następną komórką i  usuwam tą następną
        delete tmp;
    }
};