#include <map>

class Solution {
public:
    bool isHappy(int n) {
        vector<int> v{};
        map<int, bool> m;
        
        while(n != 1){
            if(v.empty()){
                while(n > 0){
                    v.push_back(n%10);
                    n /= 10;
                }
            }
            
            for(int x : v){
                x *= x;
                n += x;
            }
            if(m[n])
                return false;
            m[n] = true;
            v.clear();
            
        }
        return true;
    }
};