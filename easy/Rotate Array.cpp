class Solution {
public:
    // #1
    /*
    void rotate(vector<int>& nums, int k) {
        if(nums.size() < k) k -= nums.size();
        std::rotate(nums.begin(), nums.end()-k, nums.end());
    }
    */
    
    // #2
    void rotate(vector<int>& nums, int k) {
        if(nums.size() < k) k -= nums.size();
        
        vector<int> temp{nums.begin(), nums.end()-k};

        for(int i=0,j=nums.size()-k; j<nums.size();){
            nums[i++] = nums[j++];
        }
        for(int i=k, j=0; j<temp.size();){
            nums[i++] = temp[j++];
        }
    }
};