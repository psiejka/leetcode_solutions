/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        std::vector<int> values;
        while(head!=nullptr){
            values.push_back(head->val);
            head = head->next;
        }
        return std::equal(values.begin(), values.begin()+(values.size()/2), values.rbegin());
    }
};