class Solution {
public:
    // #1
    /*
    int maxSubArray(vector<int>& nums) {
        int sum = nums[0];
        int max = nums[0];
        for(int i=1; i<nums.size(); i++){
            sum += nums[i];
            if(nums[i] > sum) {
                sum = nums[i];
            }
            if(sum > max) max = sum;
        }
        return max;
    }
    */
    
    // #2
    int maxSubArray(vector<int>& nums) {
        int sum = nums[0];
        int max = nums[0];
        for(int i=1; i<nums.size(); i++){
            sum += nums[i];
            sum = std::max(sum, nums[i]);
            max = std::max(sum, max);
        }
        return max;
    }
};