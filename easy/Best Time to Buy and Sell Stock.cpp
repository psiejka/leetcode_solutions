class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int max_profit = 0;
        if(prices.size() > 0){
            int buy_cost = prices[0];
            for(int i = 1; i<prices.size(); i++){
                if(buy_cost > prices[i]){
                    buy_cost = min(buy_cost, prices[i]);
                    continue;
                }
                else{
                    max_profit = max(max_profit, prices[i]-buy_cost);
                }
            }
        }
        return max_profit;
    }
};