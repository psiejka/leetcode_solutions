class Solution {
public:
    // #1
    /*
    void reverseString(vector<char>& s) {
        s = {s.rbegin(), s.rend()};
    }
    */
    
    // #2
    /*
    void reverseString(vector<char>& s) {
        for(unsigned int i = 0, j = s.size()-1; i<s.size()/2;)
            swap(s[i++], s[j--]);
    }
    */
	
	// #3
    void reverseString(vector<char>& s) {
        std::reverse(s.begin(), s.end());
    }
};