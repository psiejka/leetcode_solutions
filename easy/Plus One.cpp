class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        for(int i = digits.size()-1; i>=0; i--){
            // if digit is 9 let's set it to 0
            if(digits[i] == 9){
                digits[i] -= 9;
            }
            // else just incerement the value and return
            else{
                digits[i]++;
                return digits;
            }
        }
        // if you get here, it means that you iterated over every digit in vector and you incremented every digit, 
        // so you have to set first digit to 1 and push new digit
        
        digits[0] = 1;
        digits.push_back(0);
        return digits;
    }
};