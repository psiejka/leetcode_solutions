class Solution {
public:
    // #1 - very Fast but lots of space
    /*
    int hammingWeight(uint32_t n) {
        std::string s = std::bitset<32>(n).to_string();
        return std::accumulate(s.begin(), s.end(), 0, [](int val, char c) {if(c == '1') val++; return val;});
    }
    */
    
    // #1.5 - very Fast but lots of space
    
    int hammingWeight(uint32_t n) {
        return std::bitset<32>(n).count();
    }
    
    
    // #2
    /*
    int hammingWeight(uint32_t n) {
        int count = 0;
        while(n!=0){
            count += n & 1;
            n >>= 1;
        }
        return count;
    }*/
    
    // #3
    /*
    int hammingWeight(uint32_t n) {
        int count = 0;
        while(n!=0){
            n &= (n-1); // Trick to drop the lowest active bit eg. [00001011] -> [00001010] -> [00001000] -> [00000000], 
                        //so while loop last only (active bits number) times
            count++;
        }
        return count;
    }
    */
};