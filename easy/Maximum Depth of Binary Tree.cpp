/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    //Recursive
    
    //int maxDepth(TreeNode* root) {
    //    if(root == NULL)
    //        return 0;
    //    return 1 + max(maxDepth(root->left), maxDepth(root->right));
    //}
    
    //Iterative
    int maxDepth(TreeNode* root){
        if(!root)
            return 0;
        
        queue<TreeNode*> q;
        
        q.push(root);
        int depth = 0;
        while(q.size() > 0){
            depth++;
            
            for(int i = q.size()-1; i>=0; i--){
                TreeNode* t = q.front();
                q.pop();
                
                if(t->left){
                    q.push(t->left);
                }
                if(t->right){
                    q.push(t->right);
                }
            }
        }
        return depth;
    }
};