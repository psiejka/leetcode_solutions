class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> triangle;
        for(int column=0; column<numRows; column++){
            vector<int> level;
            for(int row=0; row<column+1; row++){
                if(row==0 || row==column || column==1 || column==0){
                    level.emplace_back(1);
                }
                else{
                    level.emplace_back(triangle[column-1][row-1] + triangle[column-1][row]);
                }
            }
            triangle.emplace_back(level);
        }
        return triangle;
    }
};