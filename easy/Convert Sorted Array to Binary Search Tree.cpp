/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        return createBST(nums.begin(), nums.end());
    }
    TreeNode* createBST(vector<int>::const_iterator start, vector<int>::const_iterator end){
        TreeNode* t;
        
        int size = distance(start, end);
        if(size == 0){
            return nullptr;
        } else{
            //take medium value
            t = new TreeNode(*next(start, size/2));
            t->left = createBST(start, next(start, size/2));
            t->right = createBST(next(start, size/2+1), end);
        }
        return t;
    }
};