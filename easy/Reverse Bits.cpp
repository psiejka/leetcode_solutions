#include <bitset>
#include <string>

class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        std::string bits = std::bitset<32>(n).to_string();
        bits = {bits.rbegin(), bits.rend()};
        return std::stoul(bits, nullptr, 2);
    }
};