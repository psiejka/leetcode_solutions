class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        /* //#1
        
        if(!n)
            return;

        for(int i = m, j=0; i<nums1.size(); i++, j++){
            nums1[i] = nums2[j];
        }
        sort(nums1.begin(), nums1.end());
        */
        
        /* //#2
        n--;
        m--;
        for(int i = nums1.size()-1; i>=0; i--){
            if(m < 0) nums1[i] = nums2[n--];
            else if(n < 0) nums1[i] = nums1[m--];
            else if(nums2[n] > nums1[m]){
                nums1[i] = nums2[n--];
            }
            else{
                nums1[i] = nums1[m--];
            }
        }*/
        
        /* //#3 BEST I think
        n--;
        m--;
        for(int i = nums1.size()-1; i>=0; i--){
            if(m < 0) swap(nums1[i], nums2[n--]);
            else if(n < 0) swap(nums1[i], nums1[m--]);
            else if(nums2[n] > nums1[m]){
                swap(nums1[i], nums2[n--]);
            }
            else{
                swap(nums1[i], nums1[m--]);
            }
        }*/
        
        auto n2end = nums2.rbegin();
        if(n2end == nums2.rend()) return;
        auto n1end = nums1.rbegin()+(nums1.size()-m);
        cout << *n2end;
        for(auto i = nums1.rbegin(); i!=nums1.rend(); i++){
            if(n1end == nums1.rend()) {iter_swap(i, n2end); n2end++;}
            else if(n2end == nums2.rend()) {iter_swap(i, n1end); n1end++;}
            else if(*n2end > *n1end){
                iter_swap(i, n2end);
                n2end++;
            }
            else{
                iter_swap(i, n1end);
                n1end++;
            }
        }
    }
};