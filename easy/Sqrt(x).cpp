class Solution {
public:
    // #1 - 60% - 70%
    /*
    int mySqrt(int x) {
        return std::pow(x, 0.5);
    }
    */
    
    // #2 - Binary search - 70% - 80%
    /*
    int mySqrt(int x) {
        int low = 0, high = INT_MAX;
        while(true){
            int mid = low + (high-low)/2;  // it's the same as (low+high)/2 but with int overflow protection
            if(std::pow(mid, 2) == x) return mid;       // eg. 8     2^2=4   3^2=9 
            else if(std::pow(mid, 2) > x) high = mid-1; // lower bound
            else if(std::pow(mid+1, 2) > x) return mid; // upper bound 
            else low = mid+1;
        }
        return -1;
    }
    */
    
    // #3 - Newton method = 100% - 100% - from site
    int mySqrt(int x) {
        if(x < 2) return x; 
        long current = x/2+1;
        long next = x/2;
        
        while(next < current){
            current = next;
            next = 0.5 * (current + (x/current));
        }
        return current;
    }
};