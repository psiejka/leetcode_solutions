#include <map>

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        for(auto& x : nums){
            if(!m[x]){
                m[x] = true;
            }else{
                m[x] = false;
            }
        }
        for(auto const&[key, val] : m){
            if(val){
                return key;
            }
        }
        return -1;
    }
private:
    unordered_map<int, bool> m;
};