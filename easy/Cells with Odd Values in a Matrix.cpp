class Solution {
public:
    int oddCells(int n, int m, vector<vector<int>>& indices) {
        vector<vector<int>> v(n);
        int output = 0;
        
        for(int i=0; i<n; i++){
            v[i] = vector<int>(m, 0);
        }
        for(auto ind : indices){
            for(int j = 0; j < m; j++)
                v[ind[0]][j]++;
            for(int j = 0; j < n; j++)
                v[j][ind[1]]++;
        }
        
        for(vector<int> i : v){
            for(int j : i){
                if(j % 2 != 0)
                    output++;
            }
        }
        return output;
    }
};