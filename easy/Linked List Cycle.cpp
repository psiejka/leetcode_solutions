/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    //ITERATIVE
    
    bool hasCycle(ListNode *head) {
        if(!head || !(head->next))
            return false;
        
        ListNode* fast = head->next;
        
        while(head != fast){
            head = head->next;
            if(!head || !(head->next) || !(fast->next) || !(fast->next->next)) return false;
            fast = fast->next->next;
        }
        return true;
    }
    
    
    //RECURSIVE
    /*
    bool hasCycle(ListNode *head) {
        if(!head || !(head->next))
            return false;
        ListNode* fast = head->next;
        return hasCycle(head, fast);
    }
    
    bool hasCycle(ListNode *head, ListNode* fast) {
        if(head == fast) return true;
        else if(!head || !(head->next) || !(fast->next) || !(fast->next->next)) return false;
        return hasCycle(head->next, fast->next->next);
    }
    */
};