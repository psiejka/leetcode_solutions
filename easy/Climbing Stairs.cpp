class Solution {
public:
    //RECURSIVE - TOP-DOWN
    /*
    int climbStairs(int n) {
        if(n<0) return 0;
        if(n==0) return 1;
        if(map[n].count > 0) return map[n].count;
        
        map[n].count = climbStairs(n-2) + climbStairs(n-1);
        
        return map[n].count;
    }
private:
    struct counter{
        int count = 0;
    };
    unordered_map<int, counter> map;
    */
    
    //ITERATIVE - BOTTOM-UP #1
    /*
    int climbStairs(int n) {
        vector<int> vec(n+1, 0);
        vec[0] = 1;
        vec[1] = 2;
        
        for(int i=2; i<n; i++){
            vec[i] = vec[i-2] + vec[i-1];
        }
        return vec[n-1];
    }
    */
    
    //ITERATIVE - BOTTOM-UP #2
    int climbStairs(int n) {
        int stepOne = 1;
        int stepTwo = 0;
        int output = 0;
        
        for(int i=0; i<n; i++){
            output = stepOne + stepTwo;
            stepTwo = stepOne;
            stepOne = output;
        }
        return output;
    }
};