class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        /*for(auto i = nums.begin(); i!=nums.end();){
            if(*i == 0){
                std::rotate(i, i, nums.end());
            }
            else i++;
        }*/
        int j = 0;
        for(int i = 0; i<nums.size(); i++){
            if(nums[i] != 0)
                nums[j++] = nums[i];
        }
        
        for(;j<nums.size();j++){
            nums[j] = 0;
        }
    }
};