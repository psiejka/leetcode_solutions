class Solution {
public:
    int reverse(int x) {
        if(x == INT_MIN) return 0;
        bool negative = false;
        if(x<0){
            negative = true;
            x *= -1;
        }
        int reversed = 0;
        while(x != 0){
            if(reversed > INT_MAX/10) return 0;
            reversed = reversed*10 + x%10;
            x /= 10;
        }
        return negative ? -1*reversed : reversed;
    }
};

