/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode* previous = NULL;
        ListNode* current = head;
        ListNode* next = NULL;
        
        while(current != NULL && head != NULL){
            next = current->next;
            if(current->val == val){
                if(previous != NULL){
                previous->next = next;
                }
                else{
                    head = head->next;
                    if(head == NULL)
                        return head;
                }
            } else{
                previous = current;
            }
            current = next;
        }
    return head;
    }
};