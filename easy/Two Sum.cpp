class Solution {
public:
    //~15% / 94%
    /*
    vector<int> twoSum(vector<int>& nums, int target) {

        for(int i = 0; i<nums.size(); i++){
            for(int j = i; j<nums.size(); j++){
                if(i != j)
                    if(nums[j] == target-nums[i]){
                        return {i, j};
                    }
            }
        }
        return {};
    }
    */
    
    //~60%-90% / 60%
    vector<int> twoSum(vector<int>& nums, int target) {
        for(int i=0; i<nums.size(); i++){
            int numToFind = target-nums[i];
            if(map.find(numToFind) != map.end()){
                return { map[numToFind], i };
            }
            map[nums[i]] = i;
        }
        return {};
    }
private:
    std::unordered_map<int, unsigned int> map;
};