#include <map>

class Solution {
public:
    std::string b = "balloon";
    int count = 0;
    struct IntegerWithDef{
        unsigned int i = 0;
    };
public:
    int maxNumberOfBalloons(string text) {
        std::map<char, IntegerWithDef> letters;
        for(char x : text){
            letters[x].i++;
        }
        
        while(true){
            for(int i=0; i < b.size()-1; i++){
                if(letters[b[i]].i==0){
                    return count;
                }
                letters[b[i]].i--;
            }
            count++;
        }
    return count;
    }
};