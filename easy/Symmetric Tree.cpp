/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
	/*bool checkLevel(TreeNode* rootLeft, TreeNode* rootRight){
		if(!rootLeft && !rootRight) return true;
		if((!rootLeft || !rootRight) || (rootLeft->val != rootRight->val)) return false;
		return (rootLeft->val == rootRight->val) && checkLevel(rootLeft->left, rootRight->right) && checkLevel(rootLeft->right, rootRight->left);
	}

	bool isSymmetric(TreeNode* root) {
		if(!root){
			return true;
		}
		return checkLevel(root->left, root->right);
	}*/
	bool isSymmetric(TreeNode* root) {
        if(!root)
            return true;
        
        std::queue<TreeNode*> q;

		q.push(root);
		while (q.size() > 0) {
			std::vector<int> serialized;

			int d = q.size();
			for (int i = 0; i < d; i++) {
				TreeNode* node = q.front();
				q.pop();

				if (node->left) {
					q.push(node->left);
					serialized.push_back(node->left->val);
				}
				else
					serialized.push_back(INT_MAX);
				if (node->right) {
					q.push(node->right);
					serialized.push_back(node->right->val);
				}
				else
					serialized.push_back(INT_MAX);
            }

				if (!std::equal(serialized.begin(), serialized.begin() + serialized.size() / 2, serialized.rbegin()))
					return false;
			}
		
		return true;
	}
};