/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode* newList = NULL;

        if(l1 == NULL && l2 == NULL) return newList;
        else if(l1 == NULL && l2 != NULL) return l2;
        else if(l1 != NULL && l2 == NULL) return l1;
        
        
        if(l1->val < l2->val){
            newList = l1;
            l1 = l1->next;
        } else{
            newList = l2;
            l2 = l2->next;
        }

        ListNode* newList_head = newList;
        
        while((l1 != NULL) && (l2 != NULL)){
            if(l2->val <= l1->val){
                newList->next = l2;
                l2 = l2->next;
            }
            else{
                newList->next = l1;
                l1 = l1->next;
            }
            newList = newList->next;
        }
        if(l1 == NULL){
            newList->next = l2;
        } else{
            newList->next = l1;
        }
        
        return newList_head;
    }
};