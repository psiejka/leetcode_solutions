class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int sumAr = ((double(nums.size()))/2)*(nums.size()+1); // suma arytmetyczna
        int sum = accumulate(nums.begin(), nums.end(), 0); // suma wektora
        return sumAr - sum; //różnica
    }
};