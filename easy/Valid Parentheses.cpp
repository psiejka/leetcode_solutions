#include <stack>

class Solution {
public:
    bool isValid(string s) {
        std::stack<char> stack;
        
        for(char c : s){
            if(c == '(' || c == '{' || c == '[')
                stack.push(c);
            else if(stack.size() > 0){
                if(c == ')'){
                    if(stack.top() == '(')
                        stack.pop();
                    else
                        return false;
                }
                if(c == '}'){
                    if(stack.top() == '{')
                        stack.pop();
                    else
                        return false;
                }
                if(c == ']'){
                    if(stack.top() == '[')
                        stack.pop();
                    else
                        return false;
                }
            }
            else
                return false;
        }
        
        if(stack.size() != 0){
            return false;
        }
        
        return true;
    }
};