class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        if(strs.size() < 1) return "";
        std::string output;
        int minLength = INT_MAX;
        for(int i=0; i<strs.size(); i++){
            if(strs[i].size() < minLength){
                minLength = strs[i].size();
                output = strs[i];
            }
        }
        for(int i=0; i<strs.size(); i++){
            while(!std::equal(output.begin(), output.end(), strs[i].begin())){
                output.pop_back();
            }
        }
        return output;
    }
};