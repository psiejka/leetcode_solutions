/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    /* PASSSES
    // 25%/25%
    bool isSubtree(TreeNode* s, TreeNode* t) {
        std::vector<int> serializedA;
        std::vector<int> serializedB;
        
        serialize(s, serializedA);
        serialize(t, serializedB);
        
        for(int i=0; i<serializedA.size(); i++){
            if(serializedA[i] == serializedB[0])
                if(check(serializedA, i, serializedB)) return true;
        }
        return false;
    }
    
    bool check(std::vector<int>& A, int index, std::vector<int>& B){
        for(int i = 1; i<B.size(); i++){
            if(A[++index] != B[i]) return false;
        }
        return true;
    }
    
    void serialize(TreeNode* t, std::vector<int>& v){
        if(!t){
            v.emplace_back(INT_MAX);
            return;
        }
        serialize(t->left, v);
        serialize(t->right, v);
        v.emplace_back(t->val);
    }*/
    
    
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if(!s) return false;
        if(isSame(s,t)) return true;
        
        return isSubtree(s->left, t) || isSubtree(s->right,t);
    }
    
    bool isSame(TreeNode* s, TreeNode* t){
        if(!s && !t) return true;
        if((!s || !t) || (s->val != t->val)) return false;
           
        return isSame(s->left, t->left) && isSame(s->right, t->right);
    }
    
};