#include<stdio.h>

class Foo {
public:
    Foo() {
    }

    void first(function<void()> printFirst) {
        while(lock != 0){
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        // printFirst() outputs "first". Do not change or remove this line.
        printFirst();
        lock = 1;
    }

    void second(function<void()> printSecond) {
        while(lock != 1){
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        // printSecond() outputs "second". Do not change or remove this line.
        printSecond();
        lock = 2;

    }

    void third(function<void()> printThird) {
        while(lock != 2){
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        // printThird() outputs "third". Do not change or remove this line.
        printThird();
        lock = 3;
    }
private:
    atomic<int> lock = 0;
};